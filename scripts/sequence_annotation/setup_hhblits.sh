#!/bin/sh -e
wget https://mmseqs.com/archive/4c0cd66434ce0b83ccd247053f57989fdd53d82b/hhsuite-linux-avx2.tar.gz
tar xzvf hhsuite-linux-avx2.tar.gz

ln -s cfmsa_db cfmsa_db.ffdata
LC_ALL=C sort cfmsa_db.index > cfmsa_db.ffindex
