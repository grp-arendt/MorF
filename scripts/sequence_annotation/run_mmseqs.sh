#!/bin/bash -e
COLABFOLD_DB_PATH=/storage/databases/colabfold_db_all
./mmseqs/bin/mmseqs search cfprof ${COLABFOLD_DB_PATH}/uniref30_2103_db_seq cfres tmp -s 7.5 -a -e 1
./mmseqs/bin/mmseqs convertalis cfprof ${COLABFOLD_DB_PATH}/uniref30_2103_db_seq cfres cfress75.m8
./mmseqs/bin/mmseqs filterdb cfmsa_db cf_msa_head --extract-lines 1
./mmseqs/bin/mmseqs prefixid cf_msa_head cf_msa.tsv --tsv
join <(sort -n cfmsa_db.lookup) <(sort -n cfmsa_headers.tsv) > cfmsa_db_headers.lookup
awk 'BEGIN { OFS="\t" } NR == FNR { f[$2] = $4; next; } $1 in f { $1 = f[$1]; print; }' cfmsa_db_headers.lookup cfress75.m8 > cfress75_fixed_queries.m8

