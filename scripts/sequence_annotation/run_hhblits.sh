#!/bin/sh -e
HHSUITE_DB_PATH=/storage/databases/uniref30
NCORES=128
./hhblits_lock.sh cfmsa_db cfmsa_hhblits ${NCORES} -d ${HHSUITE_DB_PATH}/UniRef30_2021_03 -n 1 -cpu 1 -E 100
