#!/bin/sh -e
wget https://mmseqs.com/archive/7ebd2e0441e5c3bdec585317c2b1c3cdbf943568/mmseqs-linux-avx2.tar.gz
tar xzvf mmseqs-linux-avx2.tar.gz
MMSEQS_FORCE_MERGE=1 ./mmseqs/bin/mmseqs tar2db msa.tar.gz cfmsa_db --output-dbtype 11 --threads 8
./mmseqs/bin/mmseqs msa2profile cfmsa_db cfprof --threads 64
