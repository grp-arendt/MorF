#!/bin/sh -ex
#SBATCH -t 02-00
#SBATCH -o /g/arendt/npapadop/cluster/milotfold-%j.out
#SBATCH -e /g/arendt/npapadop/cluster/milotfold-%j.err
#SBATCH -p gpu
#SBATCH -C gpu=A100
#SBATCH --gres=gpu:1
#SBATCH -N 1
#SBATCH --ntasks=32
#SBATCH --mem=512000

source ~/.bash_profile
module load GCC/10.2.0
module load bzip2
module load CUDA/11.1.1-GCC-10.2.0

INPUT=$1
OUTPUT=$2
shift
shift
unset CONDA_SHLVL
eval "$(conda shell.bash hook)"
conda activate /g/arendt/npapadop/repos/condas/CoFFE
export TF_FORCE_UNIFIED_MEMORY=1
mkdir -p ${OUTPUT}
colabfold_batch "$INPUT" "$OUTPUT" --stop-at-score 85
