#!/bin/bash -ex
#SBATCH -J fs_search_pdb
#SBATCH -t 10:00:00
#SBATCH -c 16
#SBATCH -N 1
#SBATCH --mem=64G
#SBATCH -o /g/arendt/npapadop/cluster/fs_search_pdb.out
#SBATCH -e /g/arendt/npapadop/cluster/fs_search_pdb.err

module load GCC
module load bzip2

FOLDSEEK="/g/arendt/npapadop/repos/foldseek/build/bin/foldseek"
QUERYDB="/scratch/npapadop/foldseek_spongilla/spongilla"
TARGETDB="/scratch/npapadop/foldseek_target/pdb"
ALIGNMENT="/scratch/npapadop/foldseek_results/pdb"
cd /scratch/npapadop

"$FOLDSEEK" search "$QUERYDB" "$TARGETDB" "$ALIGNMENT" tmpFolder -a --threads 16 -e 1000000
"$FOLDSEEK" aln2tmscore "$QUERYDB" "$TARGETDB" "$ALIGNMENT" "$ALIGNMENT"_aln_tmscore
"$FOLDSEEK" createtsv "$QUERYDB" "$TARGETDB" "$ALIGNMENT"_aln_tmscore "$ALIGNMENT"_aln_tmscore.tsv
"$FOLDSEEK" convertalis "$QUERYDB" "$TARGETDB" "$ALIGNMENT" "$ALIGNMENT"_score.tsv

module unload