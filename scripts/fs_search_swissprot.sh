#!/bin/bash -ex
#SBATCH -J fs_search_swissprot
#SBATCH -t 3:00:00
#SBATCH -c 16
#SBATCH -N 1
#SBATCH --mem=64G
#SBATCH -o /g/arendt/npapadop/cluster/fs_search_swissprot.out
#SBATCH -e /g/arendt/npapadop/cluster/fs_search_swissprot.err

module load GCC
module load bzip2

FOLDSEEK="/g/arendt/npapadop/repos/foldseek/build/bin/foldseek"
QUERYDB="/scratch/npapadop/foldseek_spongilla/spongilla"
TARGETDB="/scratch/npapadop/foldseek_target/swissprot"
ALIGNMENT="/scratch/npapadop/foldseek_results/swissprot"
cd /scratch/npapadop

"$FOLDSEEK" search "$QUERYDB" "$TARGETDB" "$ALIGNMENT" tmpFolder -a --threads 16 -e 1000000
"$FOLDSEEK" aln2tmscore "$QUERYDB" "$TARGETDB" "$ALIGNMENT" "$ALIGNMENT"_aln_tmscore
"$FOLDSEEK" createtsv "$QUERYDB" "$TARGETDB" "$ALIGNMENT"_aln_tmscore "$ALIGNMENT"_aln_tmscore.tsv
"$FOLDSEEK" convertalis "$QUERYDB" "$TARGETDB" "$ALIGNMENT" "$ALIGNMENT"_score.tsv

module unload