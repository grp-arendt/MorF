#!/bin/bash -ex
#SBATCH -J mkpdb
#SBATCH -t 10:00:00
#SBATCH -c 16
#SBATCH --mem=64G
#SBATCH -o /g/arendt/npapadop/cluster/fs_pdb.out
#SBATCH -e /g/arendt/npapadop/cluster/fs_pdb.err

module load GCC
module load bzip2

FOLDSEEK="/g/arendt/npapadop/repos/foldseek/build/bin/foldseek"
cd /scratch/npapadop
mkdir -p foldseek
cd foldseek

$FOLDSEEK databases PDB pdb tmp --threads 16
