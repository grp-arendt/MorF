#!/bin/bash -ex
#SBATCH -J databases
#SBATCH -t 10:00:00
#SBATCH -c 10
#SBATCH --mem=400G
#SBATCH -o /g/arendt/npapadop/cluster/databases.out
#SBATCH -e /g/arendt/npapadop/cluster/databases.err

module load aria2
module load bzip2

# Setup everything for using mmseqs locally
ARIA_NUM_CONN=8
WORKDIR="/scratch/npapadop/database/"
MMSEQS="/g/arendt/npapadop/repos/MMseqs2/build/bin/mmseqs"

cd "${WORKDIR}"

hasCommand () {
    command -v "$1" >/dev/null 2>&1
}

STRATEGY=""
if hasCommand aria2c; then STRATEGY="$STRATEGY ARIA"; fi
if hasCommand curl;   then STRATEGY="$STRATEGY CURL"; fi
if hasCommand wget;   then STRATEGY="$STRATEGY WGET"; fi
if [ "$STRATEGY" = "" ]; then
	    fail "No download tool found in PATH. Please install aria2c, curl or wget."
fi

downloadFile() {
    URL="$1"
    OUTPUT="$2"
    set +e
    for i in $STRATEGY; do
        case "$i" in
        ARIA)
            FILENAME=$(basename "${OUTPUT}")
            DIR=$(dirname "${OUTPUT}")
            aria2c --max-connection-per-server="$ARIA_NUM_CONN" --allow-overwrite=true -o "$FILENAME" -d "$DIR" "$URL" && set -e && return 0
            ;;
        CURL)
            curl -L -o "$OUTPUT" "$URL" && set -e && return 0
            ;;
        WGET)
            wget -O "$OUTPUT" "$URL" && set -e && return 0
            ;;
        esac
    done
    set -e
    fail "Could not download $URL to $OUTPUT"
}

if [ ! -f UNIREF30_READY ]; then
  downloadFile "http://wwwuser.gwdg.de/~compbiol/colabfold/uniref30_2103.tar.gz" "uniref30_2103.tar.gz"
  tar xzvf "uniref30_2103.tar.gz"
  ${MMSEQS} tsv2exprofiledb "uniref30_2103" "uniref30_2103_db"
  ${MMSEQS} createindex "uniref30_2103_db" tmp1 --remove-tmp-files 1 --threads 10
  touch UNIREF30_READY
fi

if [ ! -f COLABDB_READY ]; then
  downloadFile "http://wwwuser.gwdg.de/~compbiol/colabfold/colabfold_envdb_202108.tar.gz" "colabfold_envdb_202108.tar.gz"
  tar xzvf "colabfold_envdb_202108.tar.gz"
  ${MMSEQS} tsv2exprofiledb "colabfold_envdb_202108" "colabfold_envdb_202108_db"
  # TODO: split memory value for createindex?
  ${MMSEQS} createindex "colabfold_envdb_202108_db" tmp2 --remove-tmp-files 1 --threads 10
  touch COLABDB_READY
fi

module unload aria2
module unload bzip2
