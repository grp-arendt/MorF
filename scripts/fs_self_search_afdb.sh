#!/bin/bash -ex
#SBATCH -J fs_self_afdb
#SBATCH -t 10:00:00
#SBATCH -c 32
#SBATCH -N 1
#SBATCH --mem=128G
#SBATCH -o /g/arendt/npapadop/cluster/fs_self.out
#SBATCH -e /g/arendt/npapadop/cluster/fs_self.err

module load GCC
module load bzip2

FOLDSEEK="/g/arendt/npapadop/repos/foldseek/build/bin/foldseek"
QUERYDB="/scratch/npapadop/foldseek_target/afdb"
TARGETDB="/scratch/npapadop/foldseek_target/afdb"
ALIGNMENT="/scratch/npapadop/foldseek_results/self/self"

"$FOLDSEEK" search "$QUERYDB" "$TARGETDB" "$ALIGNMENT" tmpFolder -a --threads 16
"$FOLDSEEK" aln2tmscore "$QUERYDB" "$TARGETDB" "$ALIGNMENT" "$ALIGNMENT"_aln_tmscore
"$FOLDSEEK" createtsv "$QUERYDB" "$TARGETDB" "$ALIGNMENT"_aln_tmscore "$ALIGNMENT"_aln_tmscore.tsv
"$FOLDSEEK" convertalis "$QUERYDB" "$TARGETDB" "$ALIGNMENT" "$ALIGNMENT"_score.tsv

module unload
