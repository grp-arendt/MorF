#!/bin/bash -ex
#SBATCH -J align
#SBATCH -t 10-00
#SBATCH -c 32
#SBATCH --mem=256G
#SBATCH -o /g/arendt/npapadop/cluster/align.out
#SBATCH -e /g/arendt/npapadop/cluster/align.err

module load Anaconda3
module load GCC
module load bzip2
module load CUDA
source ~/.bash_profile
conda activate /g/arendt/npapadop/repos/condas/CoFFE

MMSEQS="/g/arendt/npapadop/repos/MMseqs2/build/bin/mmseqs"
QUERY="/g/arendt/data/spongilla_singlecell_dataset/spongilla_lacustris_Trinity.fasta.transdecoder_70AA_mediumheader.fasta"
BASE="/scratch/npapadop/"

cd "${BASE}"
colabfold_search "${QUERY}" database/ ./ --mmseqs "${MMSEQS}" --threads 32

module unload
