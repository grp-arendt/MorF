#!/bin/bash -ex
#SBATCH -J align
#SBATCH -t 10-00
#SBATCH -c 32
#SBATCH --mem=256G
#SBATCH -o /g/arendt/npapadop/cluster/align_platy.out
#SBATCH -e /g/arendt/npapadop/cluster/align_platy.err

module load Anaconda3
module load GCC
module load bzip2
module load CUDA
source ~/.bash_profile
conda activate /g/arendt/npapadop/repos/condas/milot

MMSEQS="/g/arendt/npapadop/repos/MMseqs2/build/bin/mmseqs"
QUERY="/g/arendt/npapadop/data/sequences/pdumv021_proteome.fasta"
BASE="/scratch/npapadop/"

cd "${BASE}"
colabfold_search "${QUERY}" sequence_dbs/ msas --mmseqs "${MMSEQS}" --threads 32

module unload
