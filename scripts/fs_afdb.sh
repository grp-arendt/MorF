#!/bin/bash -ex
#SBATCH -J mkafdb
#SBATCH -t 10:00:00
#SBATCH -c 16
#SBATCH --mem=64G
#SBATCH -o /g/arendt/npapadop/cluster/fs_afdb.out
#SBATCH -e /g/arendt/npapadop/cluster/fs_afdb.err

module load GCC
module load bzip2

FOLDSEEK="/g/arendt/npapadop/repos/foldseek/build/bin/foldseek"
cd /scratch/npapadop
mkdir -p foldseek
cd foldseek

$FOLDSEEK databases Alphafold/Proteome afdb tmp --threads 16
