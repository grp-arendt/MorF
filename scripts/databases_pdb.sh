#!/bin/bash -ex
#SBATCH -J create_index
#SBATCH -t 08:00:00
#SBATCH -c 64
#SBATCH --mem=200G
#SBATCH -o /g/arendt/npapadop/cluster/create_index.out
#SBATCH -e /g/arendt/npapadop/cluster/create_index.err

# build the indices for the sequence databases uniref30, PDB. This is later used for the
# MSAs of the Spongilla sequences.

cd /scratch/npapadop/database

module load MMseqs2

mmseqs databases PDB PDB tmp
mmseqs createindex PDB tmp3 --remove-tmp-files 1 --threads 64

module unload MMseqs2
