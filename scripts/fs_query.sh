#!/bin/bash -ex
#SBATCH -J mkquery
#SBATCH -t 30:00
#SBATCH -c 16
#SBATCH -N 1
#SBATCH --mem=1G
#SBATCH -o /g/arendt/npapadop/cluster/fs_mkquery.out
#SBATCH -e /g/arendt/npapadop/cluster/fs_mkquery.err

module load GCC
module load bzip2

FOLDSEEK="/g/arendt/npapadop/repos/foldseek/build/bin/foldseek"
STRUCTURES="/g/arendt/npapadop/data/spongilla_af/best_models/"
cd /scratch/npapadop
mkdir -p foldseek_spongilla
cd foldseek_spongilla

$FOLDSEEK createdb "$STRUCTURES" spongilla --threads 16
