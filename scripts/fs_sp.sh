#!/bin/bash -ex
#SBATCH -J mkswiss
#SBATCH -t 10:00:00
#SBATCH -c 16
#SBATCH --mem=64G
#SBATCH -o /g/arendt/npapadop/cluster/fs_sp.out
#SBATCH -e /g/arendt/npapadop/cluster/fs_sp.err

module load GCC
module load bzip2

FOLDSEEK="/g/arendt/npapadop/repos/foldseek/build/bin/foldseek"
cd /scratch/npapadop
mkdir -p foldseek
cd foldseek

$FOLDSEEK databases Alphafold/Swiss-Prot swissprot tmp --threads 16
