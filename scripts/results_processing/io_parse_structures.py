import os
import json
from tqdm import tqdm

import numpy as np
import pandas as pd

structure_list = '/g/arendt/npapadop/repos/coffe/data/named_models/'
metadata = '/g/arendt/npapadop/repos/coffe/data/named_metadata/'

N = len(os.listdir(structure_list))
proteins = [""] * N
plddt = {}
avg_plddt = [0.] * N

for i, protein in enumerate(tqdm(os.listdir(structure_list))):
    full_name = protein.split(".")[0]
    metadata_loc = metadata + full_name + "_scores.json"
    with open(metadata_loc, "r") as f:
        score = json.load(f)
    name = "_".join(full_name.split("_")[:3])
    proteins[i] = name
    avg_plddt[i] = np.mean(score["plddt"])
    plddt[name] = score['plddt']

# save outputs
np.savez('/g/arendt/npapadop/repos/coffe/data/spongilla_plddt.npz', **plddt)

alphafold = pd.DataFrame({"isoform": proteins, "plddt": avg_plddt})
alphafold.to_csv("/g/arendt/npapadop/repos/coffe/data/structure_predictions.csv")