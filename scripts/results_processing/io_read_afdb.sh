#!/bin/bash -ex
#SBATCH -J coffe_read_afdb
#SBATCH -t 5:00:00
#SBATCH -c 1
#SBATCH --mem=32G
#SBATCH -o /g/arendt/npapadop/cluster/io_read_afdb.out
#SBATCH -e /g/arendt/npapadop/cluster/io_read_afdb.err

module load Anaconda3
source ~/.bash_profile
conda activate /g/arendt/npapadop/repos/condas/CoFFE

python /g/arendt/npapadop/repos/coffe/scripts/results_processing/io_read_afdb.py