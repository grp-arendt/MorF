# Read/write operations for result analysis

The directory contains all scripts to rerun the processing of ColabFold and Foldseek results. The
`python` files contain the code for processing; the corresponding `.sh` scripts are used to submit
to the SLURM cluster.

### io_parse_structures.py

Will go through the predicted best models and extract the pLDDT score for each residue. It saves the
per-residue-pLDDT for each protein in a dictionary (`npz` file) and the average pLDDT in a CSV
table.

### io_read_afdb.py

Processes the Foldseek predictions against AlphaFold DB. Will return a table that lists all
isoforms, their Foldseek scores, the average pLDDT of the aligned residues, and the UniProt ID of
the target protein.

### io_read_swp.py

Processes the Foldseek predictions against SwissProt. Will return a table that lists all isoforms,
their Foldseek scores, the average pLDDT of the aligned residues, and the UniProt ID of the target
protein.

### io_read_pdb.py

Processes the Foldseek predictions against PDB. Will return a table that lists all isoforms, their
Foldseek scores, the average pLDDT of the aligned residues, and the UniProt ID of the target
protein. The UniProt ID is fetched by querying UniProt (UniProt API).