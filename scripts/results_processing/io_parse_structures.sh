#!/bin/bash -ex
#SBATCH -J coffe_parse_structures
#SBATCH -t 1:00:00
#SBATCH -c 1
#SBATCH --mem=8G
#SBATCH -o /g/arendt/npapadop/cluster/io_parse_structures.out
#SBATCH -e /g/arendt/npapadop/cluster/io_parse_structures.err

module load Anaconda3
source ~/.bash_profile
conda activate /g/arendt/npapadop/repos/condas/milot

python /g/arendt/npapadop/repos/coffe/scripts/io_parse_structures.py